#include <stdio.h>
#include <stdlib.h>

unsigned long long res;

void merge(unsigned int *tab, unsigned int *t, unsigned int pocz, unsigned int sr, unsigned int kon)
{
    unsigned int i,j,q;
    for (i=pocz; i<=kon; i++) 
        t[i]=tab[i];
    i=pocz; 
    j=sr+1; 
    q=pocz;

    while (i<=sr && j<=kon) 
    {
        if (t[i]<t[j])
        {
            tab[q++]=t[i++];
        }
        else
        {
            int temp = 1;
            tab[q++]=t[j++];
            while(j <= kon && t[j]<t[i])
            {
                temp++;
                tab[q++]=t[j++];
            }
            
            res += temp * (sr - i + 1);
             
        }
    }
    while (i<=sr)
    {
        tab[q++]=t[i++];
    }
}
 
void mergesort(unsigned int *tab, unsigned int *t, unsigned int pocz, unsigned int kon)
{
    unsigned int sr;
    if (pocz<kon) 
    {
        sr=(pocz+kon)/2;
        mergesort(tab, t, pocz, sr);
        mergesort(tab, t, sr+1, kon);
        merge(tab, t, pocz, sr, kon);
    }
}


int main()
{
    unsigned int n;
    scanf("%u", &n);
    unsigned int t[n];
    unsigned int tab[n];
    for(unsigned int i = 0; i < n; i++)
    {
        scanf("%u", &tab[i]);
    }
    mergesort(tab, t, 0, n-1);
    printf("%llu\n", res); 
    return 0;
}

