**Zadanie**

Dana jest permutacja czyli n różnych liczb ze zbioru {1,2,...,n}. Oblicz ile znajduje się w niej inwersji, czyli takich par liczb (a, b), że: a > b oraz w danej permutacji liczba a znajduje się przed liczbą b.

**Wejście**

W pierwszym wierszu wejścia znajduje się liczba n (1 ≤ n ≤ 106). W drugim wierszu znajduje się dana permutacja w postaci n liczb oddzielonych pojedynczym odstępem.

**Wyjście**

Twój program powinien wypisać tylko liczbę inwersji w danej permutacji.

**Przykład**

Dla danych wejściowych

6

4 3 1 6 5 2

poprawną odpowiedzią jest

8